from django import forms

class NameForm(forms.Form):
	name = forms.CharField()

	# returns the name
	def getName(self):
		return self.cleaned_data["name"]

	# A validator method for a specific field will always be called clean_fieldname

	def clean_name(self):
		name = self.cleaned_data.get("name", "")
		if len(name) < 3:
			raise forms.ValidationError("Make name at least 3 letters long")

		return name


