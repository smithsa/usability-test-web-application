from django.db import models
from django.forms import ModelForm
from django.contrib.auth.models import User

class Project(models.Model):
	project 		= models.AutoField(primary_key=True)
	userID	 	= models.ForeignKey(User)
	name 			= models.CharField(max_length=160)
	description = models.TextField()

	def __unicode__(self):
		return u'%s %s %s' % (self.email, self.name, self.description)

class Test(models.Model):
	TEST_METRICS = (
	('C', 'Completion Rates'),
	('E', 'Error Rates'),
	('T', 'Time on Task'),
	)
	test 	= models.AutoField(primary_key=True)
	project	= models.ForeignKey('Project')
	name 	= models.CharField(max_length=200)
	description = models.TextField()
	test_metrics = models.CharField(max_length=3, choices=TEST_METRICS)

	def __unicode__(self):
		return u'%s %s %s' % (self.name, self.description, self.test_metrics)

class TestSession(models.Model):
	participant_number 	= models.AutoField(primary_key=True)
	participant 	= models.ForeignKey('Participant')
	test 		= models.ForeignKey('Test')
	date 		= models.DateField()

	def __unicode__(self):
		return u'%s %s' % (self.date, self.test)

class Task(models.Model):
	test		= models.ForeignKey('Test')
	task 		= models.AutoField(primary_key=True)
	content 	= models.TextField()

	def __unicode__(self):
		return u'%s' % (self.content)

class Answer(models.Model):
	task		= models.ForeignKey('Task')
	content 	= models.TextField()

	def __unicode__(self):
		return u'%s' % (self.content)

class Participant(models.Model):
	participant 	= models.AutoField(primary_key=True)
	first_name 		= models.CharField(max_length=30)
	last_name		= models.CharField(max_length=40)
	email 			= models.EmailField()
	phone 			= models.CharField(max_length=15)
	notes				= models.TextField()

	def __unicode__(self):
		return u'%s %s %s %s' % (self.first_name, self.last_name, self.email, self.phone)


class CompletionRates(models.Model):
	COMPLETION_RESPONSE = (
	(1, 'Success'),
	(0, 'Failure'),
	)
	task 		= models.ForeignKey('Task')
	completion_response = models.CharField(max_length=1, choices= COMPLETION_RESPONSE )

	
class ErrorRates(models.Model):
	ERROR_TYPE = (
	('C', 'Critical Error'),
	('N', 'Non-critical Error'),
	('X', 'No Error'),
	)
	task 		= models.ForeignKey('Task')
	error_type = models.CharField(max_length=1, choices= ERROR_TYPE)


class TOT(models.Model):
	task 		= models.ForeignKey('Task')
	tot 		= models.CharField(max_length=15)



class AlternateResponses(models.Model):
	task 		= models.ForeignKey('Task')
	alternateResponse	= models.CharField(max_length=1500)

	def __unicode__(self):
		return u'%s' % (self.alternateResponse)

class Notes(models.Model):
	test 		= models.ForeignKey('Task')
	notes		= models.CharField(max_length=1500)

	def __unicode__(self):
		return u'%s' % (self.notes)

####################################################################

class ProjectForm(ModelForm):
    class Meta:
        model = Project

class TestForm(ModelForm):
    class Meta:
        model = Test

class TestSessionForm(ModelForm):
    class Meta:
        model = TestSession

class TaskForm(ModelForm):
    class Meta:
        model = Task

class AnswerForm(ModelForm):
    class Meta:
        model = Answer

class ParticipantForm(ModelForm):
    class Meta:
        model = Participant

class CompletionRatesForm(ModelForm):
    class Meta:
        model = CompletionRates

class ErrorRatesForm(ModelForm):
    class Meta:
        model = ErrorRates

class TOTForm(ModelForm):
    class Meta:
        model = TOT

class AlternateResponsesForm(ModelForm):
    class Meta:
        model = AlternateResponses

class NotesForm(ModelForm):
    class Meta:
        model = Notes


