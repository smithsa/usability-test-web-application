from django.http import HttpResponse
from django.contrib.auth.decorators import permission_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
import datetime
import models
from forms import NameForm
from django.db import models
from django.shortcuts import render_to_response

def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)

def project(request):

	 return HttpResponse("<input/> <br /> <input type='submit'/>")

def display_meta(request):
    values = request.META.items()
    values.sort()
    html = []
    for k, v in values:
        html.append('<tr><td>%s</td><td>%s</td></tr>' % (k, v))
    return HttpResponse('<table>%s</table>' % '\n'.join(html))

def search_form(request):
    return render_to_response('search_form.html')

def search(request):
    error = False
    if 'q' in request.GET and request.GET['q']:
        message = 'You searched for: %r' % request.GET['q']
 	q = request.GET['q']
        if not q:
            return render_to_response('search_form.html', {'error': False})
        elif len(q) > 20:
            return render_to_response('search_form.html', {'error': False})
        else:
            return HttpResponse(message)
    else:
        message = 'You submitted an empty form.'
	return render_to_response('search_form.html', {'error': True})

def formView(request):
   f = NameForm(request.GET)
   if f.is_valid():
	#f.save()
	return render_to_response("welcome.html", {"name": f.getName()})
   return render_to_response("form.html", {"form": f})
