def is_empty(string):
	""" Function:		Checks to see if a sting is filled with black spaces.
		 Parameters:	A string
		 Returns:		A boolean, True if the string contains only black spaces. False otherwise.
	 """
	return_val = True
	for char in string:		
		if char != " ":
			return False

	return True

def main():
	print is_empty("     ")
if __name__ == "__main__":
	main()

