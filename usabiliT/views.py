from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.middleware import csrf
from forms import *
from django.db import models
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.forms.formsets import formset_factory, BaseFormSet
from SupplementalFunctions import *
from UsabilityScript import returnScript 
import datetime


def index(request):
    """ The landing page of the site. If user is already logged in they are taken
         to a page with their projects. If they are not logged in they get the landing page. """

    if(request.user.username == ""):
        return render_to_response("usabiliT/index.html")
    else:
        return HttpResponseRedirect('accounts/profile/')
    
@login_required
def projects(request):
    """ The page the user will see once signed in. Here all projects
         the user owns will be displayed. The url for this method is accounts/profile/ """
    
    username = request.user.username
    do_projects_exist = True 
    try:    
        project_list = []
        projects = Project.objects.filter(username=username)
        for project in projects:
            project_list.append(project)
    
    except:
        do_projects_exist = False 
    
    #Handle blank-slate message
    csrfContext = RequestContext(request)
    page_data = {"username":username, "project_list" :project_list, "do_projects_exist":do_projects_exist}
    return render_to_response("usabiliT/projects.html", page_data, csrfContext)

@login_required
def add_project(request):
    """ The page displays a form for the user to add a project. The
         information the user enters will then be submitted to the database.
         The user is then redirected to a page specifically for that project. """
    project_username = request.user.username
    project_obj = Project(username=project_username)

    csrfContext = RequestContext(request)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project_obj)
        if form.is_valid() and is_empty(request.POST.get('name', '')) == False: 
            form.save()
            return HttpResponseRedirect('accounts/profile/')
    else:
        form = ProjectForm()
    
    return render_to_response("usabiliT/add_project.html", {"form": form}, csrfContext)

@login_required
def delete_project(request):
    #add dome type of confirmation
    """ A deletion of a project object happens here. The project id is retrieved from post.
         The page will be a simple redirect to the displaying of the projects page."""
    csrfContext = RequestContext(request)
    try:
        project_id = request.POST.get('project_delete', '')
        project_obj = Project.objects.get(id=project_id)
        project_name = project_obj.name
        project_obj.delete()
        return HttpResponseRedirect('accounts/profile/')
    except:
        return render_to_response("usabiliT/delete_error.html", csrfContext)

@login_required
def project_page(request):
    """Page that displays the tests of the project in addition to some other stuff.
        Awkardly uses session variables to handle the association of the project id so 
        data from the database can be pulled. """
    project_name = ""
    project_description = ""
    test_list = []
    username = request.user.username
    do_tests_exist = True   

    if request.POST.get("project_select", "") == "":
        project_id = request.session['project_id']
        request.session['current_project'] = project_id
    else:
        project_id = request.POST.get("project_select", "")
        request.session['current_project'] = project_id

    project_obj = Project.objects.get(id=project_id)
    project_name = project_obj.name
    project_description = project_obj.description

    try:
        tests = UTest.objects.filter(project_id=project_id) 
        for test in tests:
            test_list.append(test)
    except:
        do_tests_exist = False
        # do a blank-slate message
    
    csrfContext = RequestContext(request)
    page_data = {"project_id":project_id,"project_name":project_name, "project_description":project_description, 
                     "test_list":test_list, "username":username
                    }
    return render_to_response("usabiliT/project_page.html", page_data, csrfContext)
    
@login_required
def edit_project(request):
    """ Page that allows the user to edit the details of the current project they have selected."""
    project_id  = request.POST.get('edit_project_link', '')
    csrfContext = RequestContext(request)
    if(project_id == ""):
        project_id = request.session['project_id']  
    else:
        request.session['project_id'] = project_id
        
    if request.method == 'POST':
        project_obj = Project.objects.get(id=project_id)
        form = ProjectForm(instance=project_obj)
        if is_empty(request.POST.get('name', '')) == False:
            form = ProjectForm(request.POST or None,  instance=project_obj)
            form.save()
            return HttpResponseRedirect("project")
    
    return render_to_response("usabiliT/edit_project.html", {"form": form, "project_id":project_id}, csrfContext)

@login_required
def add_test_page1(request):
    """ The first test page of adding a test page. """
    project_username  = request.user.username
    project_id        = request.session['current_project']
    current_date      = datetime.datetime.now()
    utest_obj         = UTest(project_id=project_id, date=current_date)
    csrfContext       = RequestContext(request)
    
    if request.method == "POST":
        form = UTestForm(request.POST or None, instance=utest_obj)
        if form.is_valid() and is_empty(request.POST.get('name', '')) == False:
            form.save()         
            request.session['current_test'] =utest_obj.id
            return HttpResponseRedirect('add-test2') # redirect to page 2 of the test
    else:
        form = UTestForm()
    formData = {"form": form, "step":"1", "step_count":"3"}
    return render_to_response("usabiliT/add_test1.html", formData, csrfContext)

@login_required
def add_test_page2(request):
    # give credit where credit is due
    """ Page will ask user if they wish to enter a script """
    test_id = request.session['current_test']
    default_script = returnScript()
    script_obj = Script(test_id=test_id)
    csrfContext = RequestContext(request)

    if request.method == "POST":
        form = ScriptForm(request.POST or None, instance=script_obj)
        if form.is_valid() and is_empty(request.POST.get('content', '')) == False:
            unsaved_script = form.save(commit=False)
            request.session['unsaved_script'] = unsaved_script
            return HttpResponseRedirect('add-test3')
    else:
        form = ScriptForm(initial={'content': default_script})

    formData = {"form": form, "step":"2", "step_count":"3"}
    return render_to_response("usabiliT/add_test2.html", formData, csrfContext)

@login_required
def add_test_page3(request): #test this shit
    class RequiredFormSet(BaseFormSet):
      def __init__(self, *args, **kwargs):
           super(RequiredFormSet, self).__init__(*args, **kwargs)
           for form in self.forms:
               form.empty_permitted = False
    
    test_id = request.session['current_test']
    TodoItemFormSet = formset_factory(TaskForm, max_num=10, formset=RequiredFormSet)
    if request.method == 'POST': 
        todo_item_formset = TodoItemFormSet(request.POST, request.FILES)
        if todo_item_formset.is_valid(): 
                for form in todo_item_formset.forms:
                    form.instance.id = test_id
                    form.save()     
                request.session['unsaved_script'].save()
                return HttpResponseRedirect('project') # Redirect to a 'success' page
    else:
      todo_item_formset = TodoItemFormSet()

    formData = {'todo_item_formset': todo_item_formset, "step":"3", "step_count":"3"}
    csrfContext = RequestContext(request)

    return render_to_response('usabiliT/add_test3.html', formData, csrfContext)

@login_required
def delete_test(request): # implement this
    """ A deletion of a test object happens here. The test id is retrieved from post.
         The page will be a simple redirect to the displaying of the specific project page."""
    csrfContext = RequestContext(request)
    try:
        if (request.POST.get('test_delete', '') == ""):
            test_id = request.session['current_test']
        else:
            test_id = request.POST.get('test_delete', '')
        test_obj = UTest.objects.get(id=test_id)
        test_name = test_obj.name
        test_obj.delete()
        return HttpResponseRedirect('project')
    except:
        return render_to_response("usabiliT/delete_error.html", csrfContext)

