from django.db import models
from django.contrib.auth.models import User
from django.forms.widgets import RadioSelect, CheckboxSelectMultiple
from SupplementalFunctions import *

class Project(models.Model):
	""" The model represents a project the user has in their account. """
	id 			   = models.AutoField(primary_key=True)
	username			= models.CharField(max_length=160)
	name 				= models.CharField(max_length=160)
	description 	= models.TextField()

	def __unicode__(self):
		return  [u'%s'% self.id, u'%s'% self.username,  u'%s'% self.name,  u'%s'% self.description],

	def clean_name(self):
		name = self.cleaned_data.get("name", "")
		if is_empty(name) == True:
			raise forms.ValidationError("Enter a name for the project")

		return name

class UTest(models.Model):
	""" The model represents a test. """
	id 			   = models.AutoField(primary_key=True)
	project_id  	= models.IntegerField()
	name 				= models.CharField(max_length=160)
	description 	= models.TextField()
	metrics 			= models.CharField(max_length=30)
	date	 			= models.CharField(max_length=30)

	def __unicode__(self):
		return  u'%s %s  %s'% (self.id, self.name, self.metrics)

	def clean_name(self):
		name = self.cleaned_data.get("name", "")
		if is_empty(name) == True:
			raise forms.ValidationError("Enter a name for the project")
		return name

class Script(models.Model):
	id 			   = models.AutoField(primary_key=True)
	test_id		  	= models.IntegerField()
	content			= models.TextField()
	def __unicode__(self):
		return  u'%s'% (self.content)

	def clean_content(self):
		content = self.cleaned_data.get("content", "")
		if is_empty(content) == True:
			raise forms.ValidationError("Enter a script")
		return content

class Task(models.Model):

	id		 			= models.AutoField(primary_key=True)
	test_id	  			= models.IntegerField()
	task_content		= models.TextField()
	correct_action		= models.TextField()


	def __unicode__(self):
		return [u'%s'% self.id, u'%s'% self.task_content, u'%s'% self.correct_action]

