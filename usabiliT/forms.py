from django import forms
from usabiliT.models import *
from django.forms import ModelForm
from django.shortcuts import render_to_response
from django.contrib.formtools.wizard import FormWizard
from django.contrib.formtools.wizard.views import SessionWizardView


class ProjectForm(ModelForm):
	"""Creating form for a project model object."""

	def __init__(self, *args, **kwargs):
		super(ProjectForm, self).__init__(*args, **kwargs)
		self.fields['description'].required = False
		
	class Meta:
		model = Project
		exclude = ("username",)

class UTestForm(ModelForm):
	"""Creating form for a test model object."""

	def __init__(self, *args, **kwargs):
		super(UTestForm, self).__init__(*args, **kwargs)
		TEST_METRICS = (
			('cmr', 'Completion Rates'),
			('err', 'Error Rates'),
		)
		self.fields['description'].required = False
		self.fields['metrics'].widget = forms.CheckboxSelectMultiple(choices=TEST_METRICS)
		
	class Meta:
		model = UTest
		exclude = ("project_id", "date",)

class ScriptForm(ModelForm):
	"""Creating form for a script model object."""

	def __init__(self, *args, **kwargs):
		super(ScriptForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Script
		exclude = ("test_id",)

class TaskForm(ModelForm):

  class Meta:
    model = Task
    exclude = ('test_id',)

class CreateTestWizard(SessionWizardView): #figure out wizardforms

 	def done(self, form_list):
		return render_to_response('usabiliT/add_test1.html', {
			'form_data': [form.cleaned_data for form in form_list],
		})

