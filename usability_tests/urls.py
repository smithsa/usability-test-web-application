from django.conf.urls import patterns, include, url
from django.conf.urls import *
from test.views import *
from testProject.views import *
from usabiliT.views import *
from registration import * 
from usabiliT.forms import CreateTestWizard, UTestForm, ScriptForm, TaskForm

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',

    # Examples:
     url(r'^test$', project),
	 url(r'^form/$',form),
     url(r'^search/$', search),
	 url(r'^editform/$', editForm),
	 url(r'^testform/$', testingform),
	 url(r'^edittestform/$', editTestForm),
	 url(r'^deleteform/$', deleteform),
	 url(r'^project/$', project),
	 url(r'^signedin/$', signedin),
	 url(r'^task/$', task_formsets),
	 url(r'^dynamic/$', index2),

	 #                      A-TEAM URLS                    #
	 ######################################################
	 url(r'^$',index),
	 url(r'^accounts/profile/$', projects),
	 url(r'^add-project$', add_project),
	 url(r'^delete-project$', delete_project),
	 url(r'^project$', project_page),
	 url(r'^edit-project$', edit_project),
	 url(r'^add-test1$', add_test_page1),
	 url(r'^add-test2$', add_test_page2),
	 url(r'^add-test3$', add_test_page3),
	 url(r'^add-test/$', CreateTestWizard.as_view([UTestForm, ScriptForm, TaskForm])),
	 url(r'^delete-test$', delete_test),

	 (r'^accounts/', include('registration.backends.default.urls')),
	 ######################################################

    #Users would then be able to register by visiting the URL /accounts/register/, login 
    #(once activated) at /accounts/login/, etc.
    
    
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
