from django import forms
from testProject import models 
from django.forms import ModelForm
from django.forms import ModelForm

class projectForm(forms.Form):
	"""More Widgets:

	from django.forms.fields import DateField, ChoiceField, MultipleChoiceField
	from django.forms.widgets import RadioSelect, CheckboxSelectMultiple
	from django.forms.extras.widgets import SelectDateWidget

	BIRTH_YEAR_CHOICES = ('1980', '1981', '1982')
	FAVORITE_COLORS_CHOICES = (('blue', 'Blue'),
                            ('green', 'Green'),
                            ('black', 'Black'))

	class SimpleForm(forms.Form):
	    birth_year = DateField(widget=SelectDateWidget(years=BIRTH_YEAR_CHOICES))
	    favorite_colors = forms.MultipleChoiceField(required=False,
		widget=CheckboxSelectMultiple, choices=FAVORITE_COLORS_CHOICES)"""


	# can set special style stuff using widget.attrs: widget=forms.TextInput(""attrs={'class':'special'})"")
	# These variable names will server as the label for the forms if I choose to autogenerate the html form.
	name 				= forms.CharField()
	description 	= forms.CharField(widget=forms.Textarea, label=None, required=False) #making the input for the field optional

	# returns the name
	def getName(self):
		return self.cleaned_data["name"]

	# returns the description
	def getDescription(self):
		return self.cleaned_data["description"]

	# Can do some serverside form validation here...easy peasy
	# simply clean_variablename
	# A validator method for a specific field will always be called clean_fieldname
	def clean_name(self):
		name = self.cleaned_data.get("name", "")
		if len(name) < 3:
			# returns the error that the user sees when validating form
			raise forms.ValidationError("Make name at least 3 letters long")

		return name

class TestForm(ModelForm):
	"""
	Creating form for a test model object.
	"""

	def __init__(self, *args, **kwargs):
		super(TestForm, self).__init__(*args, **kwargs)
		self.fields['description'].required = False

	class Meta:
		model = models.Test
		exclude = ('project')

class TaskForm(ModelForm):
	"""
	Creating form for a task model object.
	"""
	def __init__(self, *args, **kwargs):
		super(TaskForm, self).__init__(*args, **kwargs)
		self.fields['correct_action'].required = False

	class Meta:
		model = models.Task
		exclude = ('test')

		
# Changing style of form field using ModelForm
"""
class AuthorForm(ModelForm):
    class Meta:
        model = Author
        fields = ('name', 'title', 'birth_date')
        widgets = {
            'name': Textarea(attrs={'cols': 80, 'rows': 20})
        }
"""

#choice_field = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES)

class TodoListForm(ModelForm):

  class Meta:
    model = models.TodoList

class TodoItemForm(ModelForm):

  class Meta:
    model = models.TodoItem
    exclude = ('list',)
	
