# Create your views here.
from django.middleware import csrf
from django.shortcuts import render_to_response
from forms import *
from django.db import models
from django.template import RequestContext
from testProject.models import *
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.forms.formsets import formset_factory, BaseFormSet

@login_required  #prevents unauthorized users from seeing this view
def form(request):
   form = projectForm(request.POST or None)
   csrfContext = RequestContext(request)
   if form.is_valid():
		project_name = request.POST.get('name', '')
		project_description = request.POST.get('description', '')
		project_owner = request.user.username
		project_obj = Project(user=project_owner, name=project_name, description=project_description)
		project_obj.save()
		print Project.objects.all()
		return render_to_response("welcome.html", {"name": form.getName()}, csrfContext)
   return render_to_response("form.html", {"form": form}, csrfContext)

@login_required
def editForm(request):
	# when edit form alert user that the old data will be deleted or figure out a way to seperate the two
	#how will we get the id number of the object? What if the object does not exist
	#handle cases where the id does not exist...render 404??
	try:
		cur_project = Project.objects.get(id=1)
	except:
		return render_to_response("index.html")

	form = projectForm(request.POST or None, initial={'name': cur_project.name, 'description': cur_project.description})
	csrfContext = RequestContext(request)
	if form.is_valid():
		cur_project.name = request.POST.get('name', '')		
		cur_project.description = request.POST.get('description', '')		
		cur_project.save()

		return render_to_response("welcome.html", {"name": form.getName()}, csrfContext)
	return render_to_response("form.html", {"form": form}, csrfContext)

#how will we get the current project id?
#handle cases where the id does not exist...render 404?
@login_required
def testingform(request):
	"""
   Create a new Server model.
  	"""
	project_id=1
	project_obj = Project.objects.get(id=project_id)
	test_obj = Test(project=project_obj)

	csrfContext = RequestContext(request)
	if request.method == 'POST':
		form = TestForm(request.POST, instance=test_obj)
		if form.is_valid(): 
			form.save()
			return render_to_response("welcome.html", {"name": "it be working"}, csrfContext)
	else:
		form = TestForm()
 	
	
	return render_to_response("form2.html", {"form": form}, csrfContext)

#again how will we get the id number(maybe post)
# when edit form alert user that the old data will be deleted or figure out a way to seperate the two
#handle cases where the id does not exist...render 404?
@login_required
def editTestForm(request):
	test_id = 3
	test_obj = Test.objects.get(id=test_id)
	csrfContext = RequestContext(request)
	form = TestForm(request.POST or None, instance=test_obj)
	if form.is_valid():
		form.save()
		return render_to_response("welcome.html", {"name": "The editing submission has worked"}, csrfContext)
	return render_to_response("form2.html", {"form": form}, csrfContext)

def deleteform(request):
	test_id = 4
	try:
		test_obj = Test.objects.get(id=test_id)
		test_name = test_obj.name
		test_obj.delete()
		return render_to_response("delete_form.html", {"test": test_name})
	except:
		return render_to_response("welcome.html", {"name": "The test you selected does not exist"})


#Great!! AWESOME ... How to associate the projects with images!!!
def signedin(request):
	id = 2
	return render_to_response("signedin.html", {"selected_project": id})

def project(request):
	get_id = request.GET.get('id_project', '')
	return render_to_response("welcome.html", {"name": get_id})

########################################################################

def task_formsets(request):
	test_id=1
	test_obj = Test.objects.get(id=test_id)
	task_obj = Task(test=test_obj)

	csrfContext = RequestContext(request)
	if request.method == 'POST':
		form = TaskForm(request.POST, instance=test_obj)
		if form.is_valid(): 
			form.save()
			return render_to_response("welcome.html", {"name": "The Task was made!!"}, csrfContext)
	else:
		form = TaskForm()
 	
	
	return render_to_response("form3.html", {"form": form}, csrfContext)

def index2(request):
    # This class is used to make empty formset forms required
    class RequiredFormSet(BaseFormSet):
        def __init__(self, *args, **kwargs):
            super(RequiredFormSet, self).__init__(*args, **kwargs)
            for form in self.forms:
                form.empty_permitted = False
    TodoItemFormSet = formset_factory(TodoItemForm, max_num=10, formset=RequiredFormSet)
    if request.method == 'POST': # If the form has been submitted...
        todo_list_form = TodoListForm(request.POST) # A form bound to the POST data
        # Create a formset from the submitted data
        todo_item_formset = TodoItemFormSet(request.POST, request.FILES)

        if todo_list_form.is_valid() and todo_item_formset.is_valid():
            todo_list = todo_list_form.save()
            for form in todo_item_formset.forms:
                todo_item = form.save(commit=False)
                todo_item.list = todo_list
                todo_item.save()
            return HttpResponseRedirect('thanks') # Redirect to a 'success' page
    else:
        todo_list_form = TodoListForm()
        todo_item_formset = TodoItemFormSet()

    # For CSRF protection
    # See http://docs.djangoproject.com/en/dev/ref/contrib/csrf/ 
    c = {'todo_list_form': todo_list_form,
         'todo_item_formset': todo_item_formset,
        }
    csrfContext = RequestContext(request)

    return render_to_response('dynamic.html', c, csrfContext)
