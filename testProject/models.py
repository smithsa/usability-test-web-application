from django.db import models
from django.contrib.auth.models import User
from django.forms.widgets import RadioSelect, CheckboxSelectMultiple

class Project(models.Model):
	id 			   = models.AutoField(primary_key=True)
	user				= models.CharField(max_length=160)
	name 				= models.CharField(max_length=160)
	description 	= models.TextField()

	def __unicode__(self):
		return u'%s %s %s ' % (self.id, self.name, self.description)

class Test(models.Model):
	TEST_METRICS = (
	('cmr', 'Completion Rates'),
	('err', 'Error Rates'),
	('tot', 'Time on Task'),
	)
	id		 			= models.AutoField(primary_key=True)
	project  		= models.ForeignKey('Project')
	name 				= models.CharField(max_length=200)
	description 	= models.TextField()
	metrics 			= models.CharField("Test Metrics",help_text = 'What metrics do you want to test?', max_length=3, choices=TEST_METRICS)

	def __unicode__(self):
		return u'%s %s %s' % (self.id, self.name, self.description, self.test_metrics)

class Task(models.Model):

	id		 				= models.AutoField(primary_key=True)
	test		  			= models.ForeignKey('Test')
	task_content		= models.TextField()
	correct_action		= models.TextField()


	def __unicode__(self):
		return [u'%s'% self.id, u'%s'% self.task_content, u'%s'% self.correct_action]

	"""
		COMPLETION = (
		(1, 'Completed'),
		(0, 'Incomplete'),
		)

		ERROR = (
		("none", 'None'),
		("non-critical", 'Non-Critical'),
		("critical", 'Critical'),
		)

	"""

class TodoList(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class TodoItem(models.Model):
    name = models.CharField(max_length=150,
               help_text="e.g. Buy milk, wash dog etc")
    list = models.ForeignKey(TodoList)

    def __unicode__(self):
        return self.name + " (" + str(self.list) + ")"
